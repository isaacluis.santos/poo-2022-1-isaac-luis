package br.ucsal.bes.poo2021.atividade02.tui;

import br.ucsal.bes.poo2021.atividade02.domain.Veiculo;
import br.ucsal.bes.poo2021.atividade02.domain.Proprietario;
import br.ucsal.bes.poo2021.atividade02.domain.Endereco;
import java.util.Scanner;

public class Tudo {

	static final int QTD_MAX = 2000000;
	static Veiculo[] veiculos = new Veiculo[QTD_MAX];
	static Proprietario[] proprietarios = new Proprietario[QTD_MAX];
	static Integer associados = 0;
	static final String linha = "-------------------------------------------------------------------------------------------------";

	public static void main(String[] args) {
		menu();
	}

	public static void menu() {
		Scanner teclado = new Scanner(System.in);
		int v = 0, p = 0, x = 1;
		while(x!=0){
			System.out.println("\n");
			System.out.println("Digite a opção desejada:");
			System.out.println("1 - Cadastrar Veículo");
			System.out.println("2 - Listar Veículos disponíveis");
			System.out.println("3 - Listar Veículos comprados");
			System.out.println("4 - Cadastrar Proprietário");
			System.out.println("5 - Listar Proprietários (Simples)");
			System.out.println("6 - Listar Proprietários (Completo)");
			System.out.println("7 - Associar proprietário com veículo");
			System.out.println("9 - Sair");

			x = teclado.nextInt();

			System.out.println("\t");
			switch(x){
				case 1: veiculos[v] = registrarVeiculos();v++;break;
				case 2: if (v==0) {
							System.out.println("\nNão existem veículos cadastrados!\n");
						} else {
							listarVeiculosDisp(veiculos, v);
						}break;
				case 3: if (v==0 || associados==0) {
							System.out.println("\nNão existem veículos cadastrados ou associados!\n");
						} else {
							listarVeiculosIndisp(veiculos, v);
						}break;
				case 4: proprietarios[p] = cadastrarPropietario();p++;break;
				case 5: if (p==0) {
							System.out.println("\nNão existem proprietários cadastrados!\n");
						} else {
							listarProprietarios(proprietarios);
						}break;
				case 6: if (p==0) {
							System.out.println("\nNão existem proprietários cadastrados!\n");
						} else {
							listarProprietariosCompleto(proprietarios);
						}break;
				case 7: if (p==0 || v==0) {
							System.out.println("\nNão existem proprietários ou veículos cadastrados!\n");
						} else {
							associar(proprietarios, veiculos, v);
						}break;
				default: x=0;
			}
		}
	}

	public static Veiculo registrarVeiculos() {
		Scanner teclado = new Scanner(System.in);
		System.out.print("\nDigite a placa do veículo: ");
		String placa = teclado.nextLine();
		System.out.print("\nDigite o ano de fabricação do veículo: ");
		int anoFabricacao = teclado.nextInt();
		System.out.print("\nDigite o valor do veículo: ");
		Double valor = teclado.nextDouble();
		System.out.println("\n");
		Veiculo veiculo = new Veiculo(placa,anoFabricacao,valor);

		System.out.println("Registro Finalizado!");

		//Veiculo veiculo = new Veiculo("AZX-1234",2012,39000d);

		return veiculo;
	}

	public static Proprietario cadastrarPropietario(){
		Scanner teclado = new Scanner(System.in);
		System.out.print("Digite o CEP: ");
		String cep = teclado.nextLine();
		System.out.print("\nDigite o logradouro: ");
		String logradouro = teclado.nextLine();
		System.out.print("\nDigite o número do endereço: ");
		String numero = teclado.nextLine();
		System.out.print("\nDigite o complemento: ");
		String complemento = teclado.nextLine();
		System.out.print("\nDigite o bairro: ");
		String bairro = teclado.nextLine();

		Endereco enderecoPropietario = new Endereco(cep,logradouro,numero,complemento,bairro);

		System.out.print("\nDigite o seu nome: ");
		String nome = teclado.nextLine();
		System.out.print("\nDigite o seu cpf: ");
		String cpf = teclado.nextLine();
		System.out.print("\nDigite o seu telefone: ");
		String telefone = teclado.nextLine();

		Proprietario pessoa = new Proprietario(nome, cpf, telefone, enderecoPropietario);

		System.out.println("Cadastro Finalizado!");

		//Endereco enderecoPropietario = new Endereco("41730101","Cond. Brisas","6631","Torre Mar 502","Paralela");
		//Proprietario pessoa = new Proprietario("Isaac Luis", "123123121", "71982359080", enderecoPropietario);

		return pessoa;
	}

	public static void listarProprietarios(Proprietario[] proprietarios) {
		for(int i = 0; i < proprietarios.length; i++) {
			if(proprietarios[i] == null){
				break;
			}
			System.out.println(linha);
			System.out.println(i +" :\tNome: " + proprietarios[i].getNome() +" | Telefone: " + proprietarios[i].getTelefone() + " | Possuí veículos: " + proprietarios[i].getPossui());
		}
		System.out.println(linha);
		telaDeEspera();
	}

	public static void listarProprietariosCompleto(Proprietario[] proprietarios) {
		for(int i = 0; i < proprietarios.length; i++) {
			if(proprietarios[i] == null){
				break;
			}
			System.out.println(linha);
			System.out.println(i +" :\tNome: " + proprietarios[i].getNome() +" | Telefone: " + proprietarios[i].getTelefone() + " | Possuí veículos: " + proprietarios[i].getPossui() + " | CPF: " + proprietarios[i].getCpf());
			System.out.println("\tEndereço: " + proprietarios[i].getEndereco().getLogradouro() + ", " + proprietarios[i].getEndereco().getComplemento() + ", " + proprietarios[i].getEndereco().getBairro() + " " + proprietarios[i].getEndereco().getNumero() + ", " + proprietarios[i].getEndereco().getCep());
		}
		System.out.println(linha);
		telaDeEspera();
	}

	public static void listarVeiculosDisp(Veiculo[] veiculos, int qtd) {
		for(int i = 0; i < qtd; i++) {
			if(veiculos[i] == null){
				break;
			} else if (veiculos[i].getProprietario()==null) {
				System.out.println(linha);
				System.out.println(i + " :\tPlaca: " + veiculos[i].getPlaca() + " | Ano De Fabricação: " + veiculos[i].getAnoFabricacao() + " | Valor: " + veiculos[i].getValor());
			}
		}
		System.out.println(linha);
		telaDeEspera();
	}

	public static void listarVeiculosIndisp(Veiculo[] veiculos, int qtd) {
		for(int i = 0; i < qtd; i++) {
			if(veiculos[i] == null){
				break;
			} else if (veiculos[i].getProprietario()!=null) {
				System.out.println(linha);
				System.out.println(i +" :\tPlaca: " + veiculos[i].getPlaca() + " | Ano De Fabricação: " + veiculos[i].getAnoFabricacao() + " | Valor: " + veiculos[i].getValor() + " | Proprietario: " + veiculos[i].getProprietario().getNome());
			}
		}
		System.out.println(linha);
		telaDeEspera();
	}

	public static void associar(Proprietario[] pessoa, Veiculo[] carro, int qtd){
		Scanner teclado = new Scanner(System.in);
		System.out.println("Digite o index que representa o carro desejado!");
		listarVeiculosDisp(carro, qtd);
		int index1 = teclado.nextInt();
		if(carro[index1].getIndex()!=null) {
			System.out.println("Este veículo já possuí dono!");
		} else {
			System.out.println("Digite o index que representa o proprietário cadastrado!");
			listarProprietarios(pessoa);
			int index2 = teclado.nextInt();
			try {
				carro[index1].setProprietario(pessoa[index2]); // Define o proprietário do veículo;
				pessoa[index2].setPossui(1); // Variavel responsável para dizer se o usuário possuí carros ou não;
				carro[index1].setIndex(index2);pessoa[index2].setIndex(index1); // Referenciando a qual propietario o carro pertence e vice versa;
				System.out.println("Associação Realizada com Sucesso!");
				associados++;
			} catch (IndexOutOfBoundsException e) {
				System.out.println("Index não disponível!");
			}
		}
	}

	public static void telaDeEspera() {
		Scanner enter = new Scanner(System.in);
		System.out.println("Pressione Enter para voltar ao menu...");
		String qlqrCoisa = enter.nextLine();
	}
}
