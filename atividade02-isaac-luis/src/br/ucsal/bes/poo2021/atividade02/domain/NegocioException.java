package br.ucsal.bes.poo2021.atividade02.domain;

public class NegocioException extends Exception{
    public NegocioException(String message){
        super(message);
    }
}
