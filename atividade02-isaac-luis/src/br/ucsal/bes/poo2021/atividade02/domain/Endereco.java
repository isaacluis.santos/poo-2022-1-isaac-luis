package br.ucsal.bes.poo2021.atividade02.domain;

public class Endereco {
    private String cep;
    private String logradouro;
    private String numero;
    private String complemento;
    private String bairro;

    public Endereco(String cep, String logradouro, String numero, String complemento, String bairro) {
        this.cep = cep;
        this.logradouro = logradouro;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) throws NegocioException {
        if(cep.isBlank() || cep.isEmpty()){
            throw new NegocioException("Insira um cep válido!");
        }
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) throws NegocioException {
        if(logradouro.isBlank() || logradouro.isEmpty()) {
            throw new NegocioException("Insira um logradouro válido!");
        }
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) throws NegocioException {
        if(numero.isBlank() || numero.isEmpty()) {
            throw new NegocioException("Insira um número válido!");
        }
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) throws NegocioException {
        if(complemento.isBlank() || complemento.isEmpty()) {
            throw new NegocioException("Insira um complemento válido!");
        }
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) throws NegocioException {
        if(bairro.isBlank() || bairro.isEmpty()) {
            throw new NegocioException("Insira um bairro válido!");
        }
        this.bairro = bairro;
    }
}
