package br.ucsal.bes.poo2021.atividade02.domain;

public class Proprietario {
    private String nome;
    private String cpf;
    private Endereco endereco;
    private String telefone;
    private Integer possui=0;
    private Integer index;

    public Proprietario(String nome, String cpf, String telefone, Endereco endereco) {
        this.nome = nome;
        this.cpf = cpf;
        this.telefone = telefone;
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) throws NegocioException {
        if(nome.isBlank() || nome.isEmpty()) {
            throw new NegocioException("Insira um nome válido!");
        }
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) throws NegocioException {
        if(cpf.isBlank() || cpf.isEmpty()){
            throw new NegocioException("Insira um cpf válido!");
        }
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) throws NegocioException {
        if(telefone.isBlank() || telefone.isEmpty()) {
            throw new NegocioException("Insira um telefone válido!");
        }
        this.telefone = telefone;
    }

    public String getPossui() {
        if(this.possui==0) {
            return "Não";
        }
        return "Sim";
    }

    public void setPossui(Integer valor) {
        this.possui = valor;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer valor) {
        this.index = valor;
    }
}
