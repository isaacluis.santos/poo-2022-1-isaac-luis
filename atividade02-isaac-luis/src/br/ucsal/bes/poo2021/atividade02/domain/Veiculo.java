package br.ucsal.bes.poo2021.atividade02.domain;

import java.util.*;

public class Veiculo {
    private String placa;
    private Integer anoFabricacao;
    private Double valor;
    private Proprietario proprietario;
    private Integer index;

    public Veiculo(String placa, int anoFabricacao, Double valor) {
        this.placa = placa;
        this.anoFabricacao = anoFabricacao;
        this.valor = valor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) throws NegocioException {
        if(placa.isBlank() || placa.isEmpty()) {
            throw new NegocioException("Insira uma placa válida!");
        }
        this.placa = placa;
    }

    public Integer getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(Integer anoFabricacao) throws NegocioException {
        int ano = Calendar.getInstance().get(Calendar.YEAR);
        if(anoFabricacao < 0 || anoFabricacao <= 1900 || anoFabricacao > ano) {
            throw new NegocioException("Insira um ano de fabricação válido!");
        }
        this.anoFabricacao = anoFabricacao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) throws NegocioException {
        if(valor < 0) {
            throw new NegocioException("Insira um valor válido!");
        }
        this.valor = valor;
    }

    public Proprietario getProprietario() {
        return proprietario;
    }

    public void setProprietario(Proprietario proprietario) {
        this.proprietario = proprietario;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer valor) {
        this.index = valor;
    }
}
